package interpreter;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import model.QueryType;
import model.SqlDefault;

/**
 * class for translate text or code.
 * 
 * @author Francesco
 *
 */
public class InterpreterImpl implements Interpreter {

    private static final String SPACE = " ";

    /**
     * constructor.
     * 
     * @param path
     *            where find resource bundle
     * @param language
     *            An ISO 639 alpha-2 or alpha-3 language code, or a language subtag
     *            up to 8 characters in length. See the Locale class description
     *            about valid language values.
     * @param country
     *            An ISO 3166 alpha-2 country code or a UN M.49 numeric-3 area code.
     *            See the Locale class description about valid country values.
     */
    public InterpreterImpl(final String language, final String country) {
        
    }

    /**
     * constructor of InterpreterImpl with default Language and Country tacked with
     * "Locale.getDefault()" method.
     * 
     * @param path
     *            where find the file of translation
     */
    public InterpreterImpl(final String path) {
        
    }

    private String getDef(final String string) {
        // TODO Auto-generated method stub
        if (string.equals(SqlDefault.MAX_EDIT.toString())) {
                return "255";
            }
            if (string.equals(SqlDefault.CREATE_TABLE_OPEN_BRACKET_TYPE.toString())) {
                return "(";
            }
            if (string.equals(SqlDefault.CREATE_TABLE_CLOSE_BRACKET_TYPE.toString())) {
                return ")";
            }
            if (string.equals(SqlDefault.SQL_CLOSE_POINT.toString())) {
                return ";";
            }
            if (string.equals(SqlDefault.GET_DATABASE_LIST.toString())) {
                return "SELECT datname FROM pg_database WHERE datistemplate = false;";
            }
            if (string.equals(SqlDefault.GET_TABLE_LIST.toString())) {
                return "SELECT table_name FROM information_schema.tables WHERE table_schema = 'PUBLIC' ORDER BY table_name;";
            }
            if (string.equals(SqlDefault.GET_COLUMN_LIST.toString())) {
                return "SELECT column_name FROM information_schema.columns WHERE table_name  = '";
            }
            if (string.equals(SqlDefault.GET_TUPLE_LIST.toString())) {
                return "SELECT * FROM ";
            }
            if (string.equals(SqlDefault.ALTER_DATABASE.toString())) {
                return "ALTER DATABASE";
            }
            if (string.equals(SqlDefault.RENAME_TO.toString())) {
                return "RENAME TO";
            }
            if (string.equals(SqlDefault.DROP_DATABASE_IF_EXISTS.toString())) {
                return "DROP DATABASE IF EXISTS";
            }
            if (string.equals(SqlDefault.SQL_COMMA.toString())) {
                return ",";
            }
            if (string.equals(SqlDefault.CREATE_TABLE_IF_NOT_EXISTS.toString())) {
                return "CREATE TABLE";
            }
            if (string.equals(SqlDefault.DROP_TABLE_IF_EXISTS.toString())) {
                return "DROP TABLE";
            }
            if (string.equals(SqlDefault.INSERT_INTO.toString())) {
                return "INSERT INTO";
            }
            if (string.equals(SqlDefault.VALUES.toString())) {
                return "VALUES ";
            }
            if (string.equals(SqlDefault.DELETE.toString())) {
                return "DELETE ";
            }
            if (string.equals(SqlDefault.FROM.toString())) {
                return "FROM";
            }
            if (string.equals(SqlDefault.WHERE.toString())) {
                return "WHERE";
            }
            if (string.equals(SqlDefault.SELECT.toString())) {
                return "SELECT";
            }
            if (string.contains("_")) {
                return string.replace("_", " ");
            }
        return string;
    }

    @Override
    public String getQueryCompositor(final QueryType queryType, final List<String> list) {
        //System.out.println(this.getClass() + " lista:: " + list);
        String query = "";
        if (queryType.equals(QueryType.FREE_QUERY_INTERROGATION) || queryType.equals(QueryType.FREE_QUERY_UPDATE)) {
            query = list.get(0);
        } else {
            switch (queryType) {
            case GET_DATABASE_LIST:
                query = this.get(SqlDefault.GET_DATABASE_LIST.toString());
                break;
            case GET_TABLE_LIST:
                query = this.get(SqlDefault.GET_TABLE_LIST.toString());
                break;
            case GET_COLUMN_LIST:
                query = this.get(SqlDefault.GET_COLUMN_LIST.toString());
                query += list.get(0) + "'";
                break;
            case GET_TUPLE_LIST:
                query = this.get(SqlDefault.GET_TUPLE_LIST.toString());
                query += list.get(0);
                break;
            case CREATE_DATABASE:
                query = this.get(SqlDefault.CREATE_DATABASE_IF_NOT_EXISTS.toString());
                query += SPACE;
                query += list.get(0);
                query += this.get(SqlDefault.SQL_CLOSE_POINT.toString());
                break;
            case ALTER_DATABASE:
                query = this.get(SqlDefault.ALTER_DATABASE.toString());
                query += SPACE;
                query += list.get(0);
                query += SPACE;
                query += this.get(SqlDefault.RENAME_TO.toString());
                query += SPACE;
                query += list.get(1);
                query += this.get(SqlDefault.SQL_CLOSE_POINT.toString());
                break;
            case DROP_DATABASE:
                query = this.get(SqlDefault.DROP_DATABASE_IF_EXISTS.toString());
                query += SPACE;
                query += list.get(0);
                query += this.get(SqlDefault.SQL_CLOSE_POINT.toString());
                break;
            case CREATE_TABLE:
                query = this.get(SqlDefault.CREATE_TABLE_IF_NOT_EXISTS.toString());
                query += SPACE;
                query += list.get(0);
                list.remove(0);
                query += SPACE;
                query += this.get(SqlDefault.CREATE_TABLE_OPEN_BRACKET_TYPE.toString());
                for (final String string : list) {   
                    if (!string.contains(this.get(SqlDefault.CREATE_TABLE_OPEN_BRACKET_TYPE.toString()))) {
                        query += SPACE;
                    }
                    query += this.get(string);                    
                }
                query += this.get(SqlDefault.CREATE_TABLE_CLOSE_BRACKET_TYPE.toString());
                query += this.get(SqlDefault.SQL_CLOSE_POINT.toString());
                break;
            case DROP_TABLE:
                query = this.get(SqlDefault.DROP_TABLE_IF_EXISTS.toString());
                query += SPACE;
                query += list.get(0);
                query += this.get(SqlDefault.SQL_CLOSE_POINT.toString());
                break;
            case INSERT_TUPLE:
                query = this.get(SqlDefault.INSERT_INTO.toString());
                query += SPACE;
                query += list.get(0);
                list.remove(0);
                query += this.get(SqlDefault.CREATE_TABLE_OPEN_BRACKET_TYPE.toString());
                for (int i = 0; i < list.size() / 2; i++) {
                    if (list.get(i).equals(SqlDefault.SQL_COMMA.toString())) {
                        query += this.get(list.get(i));
                    } else {
                        query += list.get(i);
                    }
                }
                query += this.get(SqlDefault.CREATE_TABLE_CLOSE_BRACKET_TYPE.toString());
                query += SPACE;
                query += this.get(SqlDefault.VALUES.toString());
                query += this.get(SqlDefault.CREATE_TABLE_OPEN_BRACKET_TYPE.toString());
                for (int i = (list.size() / 2); i < list.size(); i++) {
                    if (list.get(i).equals(SqlDefault.SQL_COMMA.toString())) {
                        query +=  this.get(list.get(i));
                    } else {
                        query += "" + list.get(i) + "";
                    }
                }
                query += this.get(SqlDefault.CREATE_TABLE_CLOSE_BRACKET_TYPE.toString());
                query += this.get(SqlDefault.SQL_CLOSE_POINT.toString());
                break;
            case DELETE_TUPLE:
                query = this.get(SqlDefault.DELETE.toString());
                query += SPACE;
                query += this.get(SqlDefault.FROM.toString());
                query += SPACE;
                query += list.get(0);
                query += SPACE;
                query += this.get(SqlDefault.WHERE.toString());
                query += SPACE;
                query += list.get(1); //id
                query += this.get(SqlDefault.EQUAL.toString());
                query += "'" + list.get(2) + "'";
                query += this.get(SqlDefault.SQL_CLOSE_POINT.toString());
                break;
            case INTERROGATION_QUERY:
                query = this.get(SqlDefault.SELECT.toString());
                query += SPACE;
                query += list.get(0);
                query += SPACE;
                query += this.get(SqlDefault.FROM.toString());
                query += SPACE;
                query += list.get(1);
                if (!list.get(2).equals("")) {
                    query += SPACE;
                    query += this.get(SqlDefault.WHERE.toString());
                    query += SPACE;
                    query += list.get(2);
                }
                query += this.get(SqlDefault.SQL_CLOSE_POINT.toString());
                break;
            default:
                break;
            }
        }
        System.out.println(query);
        return query;
    }

    @Override
    public String get(String string) {
        // TODO Auto-generated method stub
        return getDef(string);
    }

}
