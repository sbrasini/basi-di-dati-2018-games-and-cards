package interpreter;

import java.util.List;

import model.QueryType;

/**
 * interface for the translation.
 * 
 * @author Francesco
 */
public interface Interpreter {


    /**
     * 
     * @param string
     *            default string
     * @return translated string (or default string if not select no language and
     *         country)
     */
    String get(String string);
    
    /**
     * get a string from a list.
     * @param queryType type of the query to compose
     * @param list relevant string in a list
     * @return the string of the query
     */
    String getQueryCompositor(QueryType queryType, List<String> list);



}
