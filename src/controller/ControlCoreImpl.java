package controller;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import interpreter.Interpreter;
import interpreter.InterpreterImpl;
import model.Database;
import model.DatabaseImpl;
import model.QueryType;


public class ControlCoreImpl implements ControlCore {

    private String databasePath;
    private Database databaseInfo;
    private Interpreter interpreterSq = new InterpreterImpl("access", "access");
    
    @Override
    public void setDatabasePath(String text) {
        // TODO Auto-generated method stub
        this.databasePath=text;
        System.out.println(text);
        this.databaseInfo = new DatabaseImpl(this.databasePath, "", "", "", "ucanaccess");
    }

    @Override
    public List<String> getTableList() {
        final String query = getQueryCompose(QueryType.GET_TABLE_LIST, null);
        return this.databaseInfo.executeQueryListReturn(QueryType.GET_TABLE_LIST, query);
    }

    @Override
    public List<String> getColumnsList(final List<String> list) {
        // list è una lista con un solo argomento, ovvero la tabella scelta
        final String query = getQueryCompose(QueryType.GET_COLUMN_LIST, list);
        return this.databaseInfo.executeQueryListReturn(QueryType.GET_COLUMN_LIST, query);
    }

    @Override
    public void executeQuery(final QueryType queryType, final List<String> list) {
        final String query = this.getQueryCompose(queryType, list);
        this.databaseInfo.executeQuery(queryType, query);
        
    }

    @Override
    public String getQueryCompose(final QueryType queryType, final List<String> list) {
        return this.interpreterSq.getQueryCompositor(queryType, list);
    }

    @Override
    public String interpreterSql(final String text) {
        return interpreterSq.get(text);
    }
    
    @Override
    public List<String> getListRegex(final String path, final boolean b, final String regex) {
        // TODO Auto-generated method stub
        List<String> list = this.getListFromTxt(path);
        if (b && regex.equals("")) {
            // if b==false but
            final String regex2 = "[^a-zA-Z0-9_]";
            list = this.getListSplitRegex(list, regex2);
        } else if (b) {
            list = this.getListSplitRegex(list, regex);
        }
        return list;
    }
    

    private List<String> getListFromTxt(String path) {
        // TODO Auto-generated method stub
        List<String> list = new LinkedList<>();
        list.add("int");
        list.add("long");
        list.add("char");
        list.add("date");            
        return list;
    }

    private List<String> getListSplitRegex(final List<String> l, final String regex) {
        // elimina qualsiasi carattere tranne "a-zA-Z" e "_"
        return Collections
                .unmodifiableList(l.stream().flatMap(s -> Stream.of(s.split(regex))).collect(Collectors.toList()));
    }
    
}
