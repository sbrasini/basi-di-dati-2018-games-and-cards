package controller;

import java.util.List;


import model.QueryType;


public interface ControlCore {

    void setDatabasePath(String text);

    /**
     * after set the database name, return list of all table.
     * 
     * @return list of all table in a database
     */
    List<String> getTableList();

    /**
     * after set the database name, return list of all column list.
     * 
     * @param list
     *            list of one element: the table name
     * @return list of all column in a table
     */
    List<String> getColumnsList(List<String> list);

    void executeQuery(QueryType insertTuple, List<String> listTuples);

    /**
     * method for compose the query.
     * 
     * @param queryType
     *            enum
     * @param list
     *            list of string, for compose query
     * @return full composed query
     */
    String getQueryCompose(QueryType queryType, List<String> list);
    /**
     * return the translated sql text.
     * 
     * @param text
     *            standard string
     * @return translated string
     */
    String interpreterSql(String text);
    
    /**
     * list of string form a txt, filter with a regex.
     * 
     * @param path
     *            of txt
     * @param b
     *            for applying default regex
     * @param regex
     *            regular expression, for filter character
     * @return the list of available language
     */
    List<String> getListRegex(String path, boolean b, String regex);
    
}
