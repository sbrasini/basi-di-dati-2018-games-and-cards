package model;

import java.util.List;

/**
 * Interfaccia che modella un oggetto di tipo Database.
 * 
 * @author silviobrasini
 *
 */
public interface Database {

    
    /**
     * Execute a query.
     * 
     * @param queryType type of the query
     * @param query the query
     */
    void executeQuery(QueryType queryType, String query);

    /**
     * Get list of all database.
     * 
     * @param queryType QueryType
     * @param query string of query
     * @return list of all databases
     */
    List<String> executeQueryListReturn(QueryType queryType, String query);

}
