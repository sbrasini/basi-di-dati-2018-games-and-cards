package model;

/**
 * sql command used in this program.
 * 
 * @author Francesco
 *
 */
public enum SqlDefault {
    /**
     * select.
     */
    SELECT("SELECT"),
    /**
     * from.
     */
    FROM("FROM"),
    /**
     * where.
     */
    WHERE("WHERE"),
    /**
     * delete.
     */
    DELETE("DELETE"),
    /**
     * equal.
     */
    EQUAL("="),
    /**
     * primary key.
     */
    PRIMARY_KEY("PRIMARY_KEY"),
    /**
     * not null.
     */
    NOT_NULL("NOT_NULL"),
    /**
     * max editable char or other.
     */
    MAX_EDIT("MAX_EDIT"),
    /**
     * open brackets in sql language.
     */
    CREATE_TABLE_OPEN_BRACKET_TYPE("CREATE_TABLE_PARENTESI_APERTA_TYPE"),
    /**
     * close brackets in sql language.
     */
    CREATE_TABLE_CLOSE_BRACKET_TYPE("CREATE_TABLE_PARENTESI_CHIUSA_TYPE"),
    /**
     * close point in sql language.
     */
    SQL_CLOSE_POINT("CREATE_TABLE_PUNTO_CHIUSURA"),
    /**
     * query for the list (List<String>) of all visible (public) database, using
     * specified sql language.
     */
    GET_DATABASE_LIST("GET_DATABASE_LIST"),
    /**
     * query for the list of all table in a database, using specified sql language.
     */
    GET_TABLE_LIST("GET_TABLE_LIST"),
    /**
     * query for the list of all column in a table, using specified sql language.
     */
    GET_COLUMN_LIST("GET_COLUMN_LIST"),
    /**
     * query for the view of all tuple in a table, using specified sql language.
     */
    GET_TUPLE_LIST("GET_TUPLE_LIST"),
    /**
     * specified sql language, for create a database.
     */
    CREATE_DATABASE_IF_NOT_EXISTS("CREATE_DATABASE_IF_NOT_EXISTS"),
    /**
     * specified sql language, for alter a database.
     */
    ALTER_DATABASE("ALTER_DATABASE"),
    /**
     * specified sql language, for rename a database.
     */
    RENAME_TO("RENAME_TO"),
    /**
     * specified sql language, for drop a database.
     */
    DROP_DATABASE_IF_EXISTS("DROP_DATABASE_IF_EXISTS"),
    /**
     * specified sql language, for create a table.
     */
    CREATE_TABLE_IF_NOT_EXISTS("CREATE_TABLE_IF_NOT_EXISTS"),
    /**
     * comma (in italiano, virgola).
     */
    SQL_COMMA("SQL_COMMA"),
    /**
     * specified sql language, for drop a table.
     */
    DROP_TABLE_IF_EXISTS("DROP_TABLE_IF_EXISTS"),
    /**
     * specified sql language, for insert a tuple.
     */
    INSERT_INTO("INSERT_INTO"),
    /**
     * specified sql language, for VALUES.
     */
    VALUES("VALUES");
    private final String sql;

    /**
     * Constructor of sql default enum.
     * 
     * @param sql
     *            string of enum
     */
    SqlDefault(final String sql) {
        // TODO Auto-generated constructor stub
        this.sql = sql;
    }

    /**
     * String of enum.
     * 
     * @return String expected
     */
    public String toString() {
        return this.sql;
    }

}
