package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JOptionPane;

import view.TableResultSet;

/**
 * Implement Database interface.
 * 
 * @author silviobrasini
 *
 */
public class DatabaseImpl implements Database {

    private final String pathDB;
    private final String nameDB;
    private final String user;
    private final String password;
    private final String typeDB;

    /**
     * Constructor of DatabaseImpl.
     * 
     * @param user username
     * @param password connection password
     * @param nameDB database name
     * @param pathDB database path
     * @param typeDB connection type
     * @param description description of the connection
     */
    public DatabaseImpl(final String pathDB, final String nameDB, final String user, final String password, final String typeDB) {
        this.password = password;
        this.user = user;
        this.nameDB = nameDB;
        this.pathDB = pathDB;
        this.typeDB = typeDB;
        
        try {// No need to explicitly load the driver if using Java >= 6 and JDBC >= 4
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
        } catch (ClassNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    @Override
    public void executeQuery(final QueryType queryType, final String query) {
        System.out.println(query);
        try {
            final Connection c = DriverManager.getConnection("jdbc:" + this.typeDB + "://" + this.pathDB + this.nameDB, this.user, this.password);
            final PreparedStatement statement = c.prepareStatement(query);
            assert c != null;

            if (queryType.equals(QueryType.INTERROGATION_QUERY) || queryType.equals(QueryType.FREE_QUERY_INTERROGATION)) {
                final ResultSet rs = statement.executeQuery();
                new TableResultSet(rs);
                rs.close();
            } else {
                statement.executeUpdate();
            }

            statement.close();
            c.close();

        } catch (SQLException e) {
            //System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    @Override
    public List<String> executeQueryListReturn(final QueryType queryType, final String query) {
        final List<String> list = new LinkedList<>();
        try (Connection c = DriverManager.getConnection("jdbc:" + this.typeDB + "://" + this.pathDB + this.nameDB, this.user, this.password);) {
            try (PreparedStatement statement = c.prepareStatement(query);) {
                assert c != null;
                final ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    list.add(rs.getString(1));
                }
                rs.close();
                statement.close();
                c.close();
            } catch (SQLException e) {
                //System.out.println(e.getMessage());
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        } catch (SQLException e) {
            //System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return list;
    }
}
