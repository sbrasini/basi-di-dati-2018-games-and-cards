package view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.ControlCore;

public class SetConnection {

    private final JFrame frame = new JFrame("carica database");
    private final JPanel panel = new JPanel();

    private final JTextField jtfPathDB = new JTextField("F:\\database\\GamesAndCards.accdb");
    private final JButton btnPathDB = new JButton("browse");
    private final JButton btnOK = new JButton("ok");

    public SetConnection(ControlCore cc) {
        BoxLayout bl = new BoxLayout(this.panel, BoxLayout.Y_AXIS);
        this.panel.setLayout(bl);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) screenSize.getWidth();
        int height = (int) screenSize.getHeight();
        screenSize.setSize(width/5, height/10);
        this.panel.setPreferredSize(screenSize);
        this.frame.setLocation(width/2, height/2);
        
        this.btnPathDB.addActionListener(new OpenFileChooser(this.jtfPathDB));
        this.btnOK.addActionListener(e->{
            cc.setDatabasePath(this.jtfPathDB.getText());
            this.frame.dispose();
        });
        
        this.frame.setResizable(false);
        this.panel.add(this.jtfPathDB);
        this.panel.add(this.btnPathDB);
        this.panel.add(this.btnOK);
        this.frame.setContentPane(this.panel);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.pack();
        this.frame.setVisible(true);
        
    }

    private class OpenFileChooser implements ActionListener {

        private final JTextField jtf;

        OpenFileChooser(final JTextField jtf) {
            this.jtf = jtf;
        }

        public void actionPerformed(final ActionEvent e) {
            final JFileChooser fileChooser = new JFileChooser();
            final int select = fileChooser.showOpenDialog(null);
            if (select == JFileChooser.APPROVE_OPTION) {
                final File file = fileChooser.getSelectedFile();
                this.jtf.setText(file.getAbsolutePath());
            }
        }
    }
}
