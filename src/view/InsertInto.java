package view;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.ControlCore;
import model.QueryType;
import model.SqlDefault;

public class InsertInto {

    private final JFrame frame = new JFrame("Games And Cards");
    private final JPanel mainPane = new JPanel(new GridLayout(3, 1));
    private final JButton ok = new JButton("ok");
    private final JLabel selectTableLabel = new JLabel();
    // lista tabelle ottenuta dal databse tramite cc
    private final JComboBox<String> tableNameComboBox = new JComboBox<>();
    // pannello delle colonne
    private JPanel columnPane = new JPanel();
    // lista di textfield,inserita in columnPane, per ottenere la tupla
    private final List<JTextField> tuplaListTextField = new LinkedList<>();

    private final List<String> tableList = new ArrayList<String>();
    // lista colonne ottenuta dal databse tramite cc
    private final List<String> columnsList = new LinkedList<String>();
    private final List<String> listMonoElement = new LinkedList<String>();

    /**
     * gui for insert the tuple.
     * 
     * @param cc
     *            ControlCore
     */
    public InsertInto(final ControlCore cc) {
        
        this.mainPane.setLayout(new BoxLayout(this.mainPane, BoxLayout.Y_AXIS));
        this.selectTableLabel.setText("SELECT_TABLE");

        cc.getTableList().forEach(this.tableList::add);
        
        
        this.tableNameComboBox.removeAllItems();
        this.tableList.forEach(this.tableNameComboBox::addItem);

        this.tableNameComboBox.addActionListener(e -> {
            this.reloadColumnPane(cc);
        });
        this.ok.addActionListener(e -> {
            final List<String> listTuples = new LinkedList<>();
            listTuples.add(this.tableList.get(this.tableNameComboBox.getSelectedIndex()));
            this.tuplaListTextField.forEach(t -> {
                if (!t.getText().equals("")) {
                    listTuples.add(this.columnsList.get(this.tuplaListTextField.indexOf(t)));
                    listTuples.add(SqlDefault.SQL_COMMA.toString());
                }
            });
            //rimuovo l'ultima virgola
            listTuples.remove(listTuples.size() - 1);
            this.tuplaListTextField.forEach(t -> {
                if (!t.getText().equals("")) {
                    listTuples.add(t.getText());
                    listTuples.add(SqlDefault.SQL_COMMA.toString());
                }
            });
            listTuples.remove(listTuples.size() - 1);
            cc.executeQuery(QueryType.INSERT_TUPLE, listTuples);
            listTuples.clear();
        });

        
        this.frame.setTitle("inserici");
        this.reloadColumnPane(cc);
        if (!this.tableList.isEmpty()) {
            this.tableNameComboBox.setSelectedIndex(0);
        }
        this.frame.setContentPane(this.mainPane);
        this.frame.pack();
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.setVisible(true);
    }

    private void reloadColumnPane(final ControlCore cc) {
        // TODO Auto-generated method stub
        this.columnsList.clear();
        this.listMonoElement.clear();
        this.tuplaListTextField.clear();
        if (!this.tableList.isEmpty()) {
            this.listMonoElement.add(this.tableList.get(this.tableNameComboBox.getSelectedIndex()));

            cc.getColumnsList(this.listMonoElement).forEach(this.columnsList::add);
            if (!this.columnsList.isEmpty()) {
                this.columnPane = new JPanel(new GridLayout(this.columnsList.size(), 1));
                this.columnsList.forEach(cl -> {
                    final JLabel nameColumnLabel = new JLabel(cl);
                    final JTextField tuplesTextField = new JTextField("");
                    this.tuplaListTextField.add(tuplesTextField);
                    this.columnPane.add(nameColumnLabel);
                    this.columnPane.add(tuplesTextField);
                });
            }
        }
        this.mainPane.removeAll();
        this.mainPane.add(this.selectTableLabel);
        this.mainPane.add(this.tableNameComboBox);
        this.mainPane.add(this.columnPane);
        this.mainPane.add(this.ok);
        this.frame.setLocationRelativeTo(null);
        this.frame.pack();
    }

}
