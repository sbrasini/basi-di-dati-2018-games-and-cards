package view;


import java.awt.GridLayout;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.LinkedList;
import java.util.List;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.ControlCore;
import model.QueryType;

public class DbManager {

    private static final String QUERY0 ="SELECT *" + 
            "FROM PERSONA";
    
    private static final String QUERY1 ="SELECT d.CFdipendente,nome,cognome,numero\r\n" + 
            "FROM DIPENDENTE d,RUBRICA r\r\n" + 
            "WHERE d.CFdipendente=r.CFdipendente";
    private static final String QUERY2 ="SELECT d.CFdipendente,nome,cognome,SUM(quantita) AS quantitaArticoli\r\n" + 
            "FROM DIPENDENTE d, VENDITA v\r\n" + 
            "WHERE d.CFdipendente=v.CFdipendente\r\n" + 
            "GROUP BY d.CFdipendente,nome,cognome\r\n" + 
            "ORDER BY SUM(quantita) DESC";
    private static final String QUERY3 ="SELECT d.CFdipendente,d.nome,d.cognome,SUM(v.quantita*a.prezzo) AS incassoTot\r\n" + 
            "FROM DIPENDENTE d , VENDITA v, ARTICOLO a\r\n" + 
            "WHERE d.CFdipendente=v.CFdipendente\r\n" + 
            "AND v.CodArticolo=a.CodArticolo\r\n" + 
            "GROUP BY d.CFdipendente,d.nome,d.cognome\r\n" + 
            "ORDER BY SUM(v.quantita*a.prezzo) DESC";
    private static final String QUERY4 ="SELECT t.CodTorneo, t.nome, SUM(quotaIscrizione) AS incassoTot\r\n" + 
            "FROM TORNEO t, PARTECIPA p\r\n" + 
            "WHERE t.CodTorneo=p.CodTorneo\r\n" + 
            "GROUP BY t.CodTorneo,t.nome\r\n" + 
            "ORDER BY SUM(quotaIscrizione) DESC";
    private static final String QUERY5 ="SELECT t.CodTorneo,t.nome,t.TipoGioco,COUNT(p.CodGioc) AS TotPartecipazioni\r\n" + 
            "FROM TORNEO t, PARTECIPA p\r\n" + 
            "WHERE t.CodTorneo=p.CodTorneo\r\n" + 
            "GROUP BY t.CodTorneo,t.nome,t.TipoGioco\r\n" + 
            "ORDER BY COUNT(p.CodGioc) DESC";
    private static final String QUERY6 ="SELECT tg.Nome,COUNT(p.CodGioc)AS giocatoriAderiti,t.quotaIscrizione\r\n" + 
            "FROM \r\n" + 
            "TIPO_GIOCO tg,\r\n" + 
            "TORNEO t,\r\n" + 
            "PARTECIPA p\r\n" + 
            "WHERE \r\n" + 
            "t.CodTorneo=p.CodTorneo \r\n" + 
            "AND t.TipoGioco=tg.Nome\r\n" + 
            "GROUP BY tg.Nome,t.quotaIscrizione";
    private static final String QUERY7 ="SELECT CodGioc,COUNT(CodTorneo) AS GIOCATO_IN_n_TORNEI\r\n" + 
            "FROM PARTECIPA\r\n" + 
            "GROUP BY CodGioC";
    private static final String QUERY8 ="SELECT g.CodGioc,g.nome,g.cognome,SUM(pr.quantita) AS QUANTITA_PREMIO\r\n" + 
            "FROM  GIOCATORE g,PARTECIPA p,TORNEO t,PREMIO pr\r\n" + 
            "WHERE g.CodGioc=p.CodGioc AND t.CodTorneo=p.CodTorneo\r\n" + 
            "AND t.CodTorneo=pr.CodTorneo AND\r\n" + 
            "p.posizioneClassifica=pr.posizione\r\n" + 
            "GROUP BY g.CodGioc,g.nome,g.cognome";
    private static final String QUERY9 ="SELECT i.CodPrenotazione,a.dataCreazione,p.data,i.CodArticolo,i.quantita AS QUANTITA_RICHESTA,Ao.qta AS QUANTITA_MAGAZZINO\r\n" + 
            "FROM PRENOTAZIONE p,\r\n" + 
            "ARTICOLO a,\r\n" + 
            "INCLUSO i,\r\n" + 
            "(\r\n" + 
            "SELECT ARTICOLO.CodArticolo AS CodArticolo,ARTICOLO.quantita AS ARTICOLOquantita,\r\n" + 
            "IIF(ISNULL(ORDINI.quantita),0,ORDINI.quantita) AS ORDINIquantita,\r\n" + 
            "IIF(ISNULL(INCLUSO.quantita),0,INCLUSO.quantita) AS INCLUSOquantita,\r\n" + 
            "IIF(ISNULL(VENDITA.quantita),0,VENDITA.quantita) AS VENDITAquantita,\r\n" + 
            "IIF(ISNULL(PREMIO.quantita),0,PREMIO.quantita) AS PREMIOquantita,\r\n" + 
            "(ARTICOLO.quantita\r\n" + 
            "+IIF(ISNULL(ORDINI.quantita),0,ORDINI.quantita) \r\n" + 
            "-IIF(ISNULL(INCLUSO.quantita),0,INCLUSO.quantita) \r\n" + 
            "-IIF(ISNULL(VENDITA.quantita),0,VENDITA.quantita) \r\n" + 
            "-IIF(ISNULL(PREMIO.quantita),0,PREMIO.quantita) \r\n" + 
            ")AS qta\r\n" + 
            "FROM\r\n" + 
            "(((\r\n" + 
            "ARTICOLO LEFT JOIN\r\n" + 
            "ORDINI\r\n" + 
            "ON\r\n" + 
            "ARTICOLO.CodArticolo=ORDINI.CodArticolo\r\n" + 
            ")\r\n" + 
            "LEFT JOIN INCLUSO\r\n" + 
            "ON\r\n" + 
            "ARTICOLO.CodArticolo=INCLUSO.CodArticolo\r\n" + 
            ")\r\n" + 
            "LEFT JOIN VENDITA\r\n" + 
            "ON\r\n" + 
            "ARTICOLO.CodArticolo=VENDITA.CodArticolo\r\n" + 
            ")\r\n" + 
            "LEFT JOIN PREMIO\r\n" + 
            "ON\r\n" + 
            "ARTICOLO.CodArticolo=PREMIO.CodArticolo\r\n" + 
            ")AS Ao\r\n" + 
            "WHERE p.CodPrenotazione=i.CodPrenotazione\r\n" + 
            "AND i.CodArticolo=ao.CodArticolo\r\n" + 
            "AND i.CodArticolo=a.CodArticolo\r\n" + 
            "AND i.quantita>Ao.qta\r\n" + 
            "AND a.dataCreazione<=Now()\r\n" + 
            "GROUP BY i.CodPrenotazione,a.dataCreazione,p.data,i.CodArticolo,i.quantita ,Ao.qta\r\n" + 
            "ORDER BY p.data";
    
    
    private static final String QUERY11 ="SELECT Ao.CodArticolo,Ao.nome,Ao.prezzo,Ao.qta,Ao.espande,Ao.manuale\r\n" + 
            "FROM \r\n" + 
            "(\r\n" + 
            "SELECT ARTICOLO.CodArticolo AS CodArticolo,\r\n" + 
            "ARTICOLO.quantita AS ARTICOLOquantita,\r\n" + 
            "IIF(ISNULL(ORDINI.quantita),0,ORDINI.quantita) AS ORDINIquantita,\r\n" + 
            "IIF(ISNULL(INCLUSO.quantita),0,INCLUSO.quantita) AS INCLUSOquantita,\r\n" + 
            "IIF(ISNULL(VENDITA.quantita),0,VENDITA.quantita) AS VENDITAquantita,\r\n" + 
            "IIF(ISNULL(PREMIO.quantita),0,PREMIO.quantita) AS PREMIOquantita,\r\n" + 
            "(ARTICOLO.quantita\r\n" + 
            "+IIF(ISNULL(ORDINI.quantita),0,ORDINI.quantita) \r\n" + 
            "-IIF(ISNULL(INCLUSO.quantita),0,INCLUSO.quantita) \r\n" + 
            "-IIF(ISNULL(VENDITA.quantita),0,VENDITA.quantita) \r\n" + 
            "-IIF(ISNULL(PREMIO.quantita),0,PREMIO.quantita) \r\n" + 
            ")AS qta,\r\n" + 
            "ARTICOLO.nome,ARTICOLO.espande,ARTICOLO.manuale,ARTICOLO.prezzo\r\n" + 
            "FROM\r\n" + 
            "(((\r\n" + 
            "ARTICOLO LEFT JOIN\r\n" + 
            "ORDINI\r\n" + 
            "ON\r\n" + 
            "ARTICOLO.CodArticolo=ORDINI.CodArticolo\r\n" + 
            ")\r\n" + 
            "LEFT JOIN INCLUSO\r\n" + 
            "ON\r\n" + 
            "ARTICOLO.CodArticolo=INCLUSO.CodArticolo\r\n" + 
            ")\r\n" + 
            "LEFT JOIN VENDITA\r\n" + 
            "ON\r\n" + 
            "ARTICOLO.CodArticolo=VENDITA.CodArticolo\r\n" + 
            ")\r\n" + 
            "LEFT JOIN PREMIO\r\n" + 
            "ON\r\n" + 
            "ARTICOLO.CodArticolo=PREMIO.CodArticolo\r\n" + 
            "GROUP BY ARTICOLO.CodArticolo,\r\n" + 
            "ARTICOLO.quantita,\r\n" + 
            "ORDINI.quantita,\r\n" + 
            "INCLUSO.quantita,\r\n" + 
            "VENDITA.quantita,\r\n" + 
            "PREMIO.quantita,\r\n" + 
            "ARTICOLO.nome,ARTICOLO.espande,ARTICOLO.manuale,ARTICOLO.prezzo\r\n" + 
            ")AS Ao\r\n" + 
            "\r\n" + 
            "WHERE \r\n" + 
            "(\r\n" + 
            "Ao.nome LIKE '*";
    private static final String QUERY12 ="*'\r\n" + 
            "or\r\n" + 
            "Ao.espande LIKE '*";
    private static final String QUERY13 ="*'\r\n" + 
            ")\r\n" + 
            "GROUP BY Ao.CodArticolo,Ao.nome,Ao.prezzo,Ao.qta,Ao.espande,Ao.manuale\r\n" + 
            "ORDER BY Ao.CodArticolo";
    
    
    private final JFrame frame = new JFrame("database manager");
    
    private final JButton btnInserisci;
    
    private final JButton numeriTelefono = new JButton("numeri di telefono associati ad ogni dipendente");
    private final JButton dipendentiVendutopiuArticoli= new JButton("dipendenti che hanno venduto più articoli");
    private final JButton dipendentiIncassatopiu= new JButton("dipendenti che hanno incassato di più");
    private final JButton torneiPiuRemunerativi= new JButton("i tornei più remunerativi");
    private final JButton torneiPiuPartecipazioni= new JButton("tornei che hanno avuto più partecipazioni");//5
    private final JButton tipGiochiPiuPartecipazioni= new JButton("tipi di giochi che hanno avuto più partecipazioni");
    private final JButton giocatoriPiuAttivi= new JButton("giocatori (più attivi) che partecipano a più tornei");
    private final JButton giocatoriVintoPiuPremi= new JButton("giocatori che hanno vinto più premi");//8
    
    private final JButton prenotazioniConArticoliNonDisponibili = new JButton("le prenotazioni i cui articoli non sono disponibili\r\n" + 
            "(articolo uscito dalla casa distribuzione)\r\n" + 
            "quindi manca nel magazziono (atricoli+ordini) e va ancora ordinato");
    //10
    private final JButton prenotazioniDisponibili= new JButton("le prenotazioni i cui articoli sono disponibili,\r\n" + 
            "pronti per il ritiro in negozio\r\n" + 
            "+ email dell' acquirente");
    
    private final JButton articoliManualiEspansioni= new JButton("(like)\r\n" + 
            "articoli con nome simile + prezzo+ quantità disponibile,\r\n" + 
            "manuali + prezzo manuali + quantità,\r\n" + 
            "espansioni + prezzo espansioni + quantità\r\n");

    private final JPanel panel;
    private final List<String> queryList = new LinkedList<>();


    
    //F:\database\GamesAndCards.accdb
    public DbManager(ControlCore cc) {
        this.panel = new JPanel(new GridLayout(0, 1));

        this.btnInserisci = new JButton("Inserisci");
        this.btnInserisci.addActionListener(e->new InsertInto(cc));
        
        this.numeriTelefono.addActionListener(e->{
            this.queryList.add(QUERY1);
            cc.executeQuery(QueryType.FREE_QUERY_INTERROGATION,this.queryList);
            this.queryList.remove(0);
        });
        this.dipendentiVendutopiuArticoli.addActionListener(e->{
            this.queryList.add(QUERY2);
            cc.executeQuery(QueryType.FREE_QUERY_INTERROGATION,this.queryList);
            this.queryList.remove(0);
        });
        this.dipendentiIncassatopiu.addActionListener(e->{
            this.queryList.add(QUERY3);
            cc.executeQuery(QueryType.FREE_QUERY_INTERROGATION,this.queryList);
            this.queryList.remove(0);
        });
        this.torneiPiuRemunerativi.addActionListener(e->{
            this.queryList.add(QUERY4);
            cc.executeQuery(QueryType.FREE_QUERY_INTERROGATION,this.queryList);
            this.queryList.remove(0);
        });
        this.torneiPiuPartecipazioni.addActionListener(e->{
            this.queryList.add(QUERY5);
            cc.executeQuery(QueryType.FREE_QUERY_INTERROGATION,this.queryList);
            this.queryList.remove(0);
        });
        this.tipGiochiPiuPartecipazioni.addActionListener(e->{
            this.queryList.add(QUERY6);
            cc.executeQuery(QueryType.FREE_QUERY_INTERROGATION,this.queryList);
            this.queryList.remove(0);
        });
        this.giocatoriPiuAttivi.addActionListener(e->{
            this.queryList.add(QUERY7);
            cc.executeQuery(QueryType.FREE_QUERY_INTERROGATION,this.queryList);
            this.queryList.remove(0);
        });
        this.giocatoriVintoPiuPremi.addActionListener(e->{
            this.queryList.add(QUERY8);
            cc.executeQuery(QueryType.FREE_QUERY_INTERROGATION,this.queryList);
            this.queryList.remove(0);
        });
        this.prenotazioniConArticoliNonDisponibili.addActionListener(e->{
            this.queryList.add(QUERY9);
            cc.executeQuery(QueryType.FREE_QUERY_INTERROGATION,this.queryList);
            this.queryList.remove(0);
        });
        this.prenotazioniDisponibili.addActionListener(e->{
            this.queryList.add(QUERY10);
            cc.executeQuery(QueryType.FREE_QUERY_INTERROGATION,this.queryList);
            this.queryList.remove(0);
        });
        JTextField field = new JTextField("");
        this.articoliManualiEspansioni.addActionListener(e->{
            String s = QUERY11+field.getText()+QUERY12+field.getText()+QUERY13;
            this.queryList.add(s);
            cc.executeQuery(QueryType.FREE_QUERY_INTERROGATION,this.queryList);
            this.queryList.remove(0);
        });
        
        this.panel.add(this.btnInserisci);
        
        this.panel.add(this.numeriTelefono);
        this.panel.add(this.dipendentiVendutopiuArticoli);
        this.panel.add(this.dipendentiIncassatopiu);
        this.panel.add(this.torneiPiuRemunerativi);
        this.panel.add(this.torneiPiuPartecipazioni);//5
        this.panel.add(this.tipGiochiPiuPartecipazioni);
        this.panel.add(this.giocatoriPiuAttivi);
        this.panel.add(this.giocatoriVintoPiuPremi);
        this.panel.add(this.prenotazioniConArticoliNonDisponibili);
        this.panel.add(this.prenotazioniDisponibili);//10
        this.panel.add(field);
        this.panel.add(this.articoliManualiEspansioni);
        
        
        this.frame.setContentPane(this.panel);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.pack();
        this.frame.setVisible(true);
        this.frame.setLocationRelativeTo(null);
    }
    private static final String QUERY10 ="SELECT p.CodPrenotazione,a.codAcq,a.email\r\n" + 
            "FROM PRENOTAZIONE p,ACQUIRENTE a,\r\n" + 
            "( SELECT a.CodPrenotazione AS CodPrenotazione\r\n" + 
            "FROM\r\n" + 
            "( SELECT CodPrenotazione, COUNT(CodArticolo) AS conta\r\n" + 
            "FROM\r\n" + 
            "( SELECT i.CodPrenotazione AS CodPrenotazione,i.CodArticolo AS CodArticolo\r\n" + 
            "FROM ARTICOLO a,INCLUSO i,\r\n" + 
            "( SELECT ARTICOLO.CodArticolo AS CodArticolo,ARTICOLO.quantita AS ARTICOLOquantita,\r\n" + 
            "IIF(ISNULL(ORDINI.quantita),0,ORDINI.quantita) AS ORDINIquantita,\r\n" + 
            "IIF(ISNULL(INCLUSO.quantita),0,INCLUSO.quantita) AS INCLUSOquantita,\r\n" + 
            "IIF(ISNULL(VENDITA.quantita),0,VENDITA.quantita) AS VENDITAquantita,\r\n" + 
            "IIF(ISNULL(PREMIO.quantita),0,PREMIO.quantita) AS PREMIOquantita,\r\n" + 
            "(ARTICOLO.quantita\r\n" + 
            "+IIF(ISNULL(ORDINI.quantita),0,ORDINI.quantita) \r\n" + 
            "-IIF(ISNULL(INCLUSO.quantita),0,INCLUSO.quantita) \r\n" + 
            "-IIF(ISNULL(VENDITA.quantita),0,VENDITA.quantita) \r\n" + 
            "-IIF(ISNULL(PREMIO.quantita),0,PREMIO.quantita) \r\n" + 
            ")AS qta\r\n" + 
            "FROM\r\n" + 
            "(((\r\n" + 
            "ARTICOLO LEFT JOIN ORDINI ON\r\n" + 
            "ARTICOLO.CodArticolo=ORDINI.CodArticolo\r\n" + 
            ") LEFT JOIN INCLUSO ON\r\n" + 
            "ARTICOLO.CodArticolo=INCLUSO.CodArticolo\r\n" + 
            ") LEFT JOIN VENDITA ON\r\n" + 
            "ARTICOLO.CodArticolo=VENDITA.CodArticolo)\r\n" + 
            "LEFT JOIN PREMIO ON\r\n" + 
            "ARTICOLO.CodArticolo=PREMIO.CodArticolo\r\n" + 
            ")AS Ao\r\n" + 
            "WHERE \r\n" + 
            "i.CodArticolo=ao.CodArticolo\r\n" + 
            "AND i.CodArticolo=a.CodArticolo\r\n" + 
            "AND i.quantita<=Ao.qta\r\n" + 
            "AND a.dataCreazione<=Now()\r\n" + 
            "GROUP BY i.CodPrenotazione, i.CodArticolo\r\n" + 
            ")GROUP BY CodPrenotazione\r\n" + 
            ") AS a,(\r\n" + 
            "SELECT i.CodPrenotazione AS CodPrenotazione,COUNT(i.CodArticolo)AS conta\r\n" + 
            "FROM PRENOTAZIONE p,INCLUSO i,ARTICOLO a\r\n" + 
            "WHERE p.CodPrenotazione=i.CodPrenotazione\r\n" + 
            "AND i.CodArticolo=a.CodArticolo\r\n" + 
            "GROUP BY i.CodPrenotazione\r\n" + 
            ") AS b\r\n" + 
            "WHERE a.CodPrenotazione=b.CodPrenotazione\r\n" + 
            "AND a.conta=b.conta\r\n" + 
            "GROUP BY a.CodPrenotazione\r\n" + 
            ") AS c\r\n" + 
            "WHERE p.CodPrenotazione=c.CodPrenotazione\r\n" + 
            "AND a.codAcq=p.codAcq";
}
