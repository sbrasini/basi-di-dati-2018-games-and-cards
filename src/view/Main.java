package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.ControlCore;
import controller.ControlCoreImpl;

/**
 * Finestra iniziale.
 *
 */
public class Main {

    private final JFrame frame = new JFrame("Games And Cards");    
    private final JButton setConnection = new JButton("setta conessione");
    private final JButton dbManagement = new JButton("manager database");

    public Main(ControlCore cc) {
        
        this.frame.setResizable(false);
        final JPanel pane = new JPanel(new FlowLayout());
        
        this.setConnection.addActionListener(e->{
            new SetConnection(cc);
        });
        this.dbManagement.addActionListener(e -> {
            new DbManager(cc);
        });
        pane.add(this.setConnection);
        pane.add(this.dbManagement);
        
        this.frame.setContentPane(pane);        
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.pack();
        this.frame.setVisible(true);
        this.frame.setLocationRelativeTo(null);


    }

    public static void main(final String[] args) {
        ControlCore cc = new ControlCoreImpl();
        new Main(cc);
    }

}
