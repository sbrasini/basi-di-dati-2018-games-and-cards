package view;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * see the record of a interrogation query.
 * @author Francesco
 *
 */
public class TableResultSet {

    private final JFrame frame = new JFrame();
    private final JPanel panel = new JPanel();
    private JTable table = new JTable();
    private final List<String> columnName = new ArrayList<>();
    private final List<Integer> columnDisplaySize = new ArrayList<>();
    private final List<String[]> tuples = new ArrayList<>();

    /**
     * gui with JScrollPane, for see resultset from database.
     * @param rs ResultSet from database
     */
    public TableResultSet(final ResultSet rs) {

       try {
            final int colCount = rs.getMetaData().getColumnCount();

            for (int i = 1; i <= colCount; i++) {
                // example ID,CF,name,surname
                this.columnName.add(rs.getMetaData().getColumnName(i));
                this.columnDisplaySize.add(rs.getMetaData().getColumnDisplaySize(i));
            }
            while (rs.next()) {
                final List<String> tuple = new ArrayList<>();
                /*
                 * Bug: new view.TableResultSet(ResultSet) attempts to access a result set field
                 * with index 0
                 * 
                 * A call to getXXX or updateXXX methods of a result set was made where the
                 * field index is 0. As ResultSet fields start at index 1, this is always a
                 * mistake.
                 * 
                 */
                for (int i = 1; i <= colCount; i++) {
                    tuple.add(rs.getString(i));
                }
                tuples.add((String[]) tuple.toArray(new String[] {}));
            }
            this.table = new JTable(this.tuples.toArray(new Object[][] {}), this.columnName.toArray());
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        final JScrollPane scrollPane = new JScrollPane(table);
        this.panel.add(scrollPane);
        this.frame.setContentPane(panel);
        this.frame.setTitle("ResultSet");
        this.frame.setLocationRelativeTo(null);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.pack();
        this.frame.setVisible(true);
    }

}
